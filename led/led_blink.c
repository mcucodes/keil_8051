/* 
* file: led_blink.c
* purpose: to blink an led
* author: mcu_codes
* website: www.mcucodes.wordpress.com
* mail: mcu.codes@gmail.com
*/

#include <reg51.h>
#define led_port P2
sbit led = P1^0;

void delay(int n) {
	while(n--);
}

void main() {
	led_port = 0x00;
	led = 0;
	while(1) {
		led_port = ~led_port;
		led = ~led;
		delay(50000);
	}
}
		
	